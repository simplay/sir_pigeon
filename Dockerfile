FROM node:14.8.0

# Create app directory
WORKDIR /usr/src/app

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package*.json ./

RUN echo "# Essentials" && \
      apt-get update && apt install -y curl wget git rsync patch build-essential software-properties-common

RUN npm install
RUN npm install typescript -g

# Copy and build
COPY . .
RUN echo "Compiling typescript to javascript..." && tsc

EXPOSE 3000

CMD [ "node", "./build/bot.js" ]
