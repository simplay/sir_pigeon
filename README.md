# SirPigeon

[![pipeline status](https://gitlab.com/simplay/sir_pigeon/badges/master/pipeline.svg)](https://gitlab.com/simplay/sir_pigeon/-/commits/master)
[![coverage report](https://gitlab.com/simplay/sir_pigeon/badges/master/coverage.svg)](https://gitlab.com/simplay/sir_pigeon/-/commits/master)

<img src="docs/heyitsmesirpigeon.png" width="300" />

## Setup

```
cp .env.example .env
```

and fill-in the missing values for the corresponding env-variables.

## Usage

### Via Docker

Execute `./run-via-docker.sh` and follow the instructions.

### Direct

```
npm install
npm run dev
```

## References

+ https://github.com/discordjs/discord.js
