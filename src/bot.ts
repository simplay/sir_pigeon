import { Client, ClientOptions, Intents } from 'discord.js';

import fs from 'fs';

require('dotenv').config();

const SIR_PIGEON_DISCORD_TOKEN = process.env.SIR_PIGEON_DISCORD_TOKEN || '';

const params = {
  intents: [Intents.FLAGS.GUILDS, Intents.FLAGS.GUILD_MESSAGES, Intents.FLAGS.GUILD_VOICE_STATES],
} as ClientOptions;

const client = new Client(params);
const mew_sounds_directory = 'audio/mew_sounds/';
const p_purr_sound = 0.75;

client.on('ready', () => {
  console.log(`Logged in as ${client.user!.tag}!`);
});

let moorhun_is_close = false;

const cat_purring_sound = `${mew_sounds_directory}purring.mp3`;

client.on('message', async message => {
  // TODO: re-enable and flag that we are chatting directly to the bad. This changes the arg parsing.
  // if (!message.guild) {
  //   console.log('Ignoring direct messages...');
  //   return;
  // }

  if (!message.content.startsWith('!')) {
    return;
  }

  // TODO: change to [cmd, target, ...args] to easily join messages
  const [cmd, ...args] = message.content.split(' ');
  console.log(`cmd: ${cmd}, args: ${args}`);

  message.delete();
  switch (cmd) {
    case '!ping':
      message.author.send('pong');
      break;
    case '!moorhuhn':
      if (message.member?.voice?.channel) {
        moorhun_is_close = true;
        setTimeout(() => {
          moorhun_is_close = false;
        }, 20_000);
        const connection = await message.member.voice.channel.join();
        const dispatcher = connection.play('audio/sfx_big_chicken_pops_up.wav');
        dispatcher.on('finish', () => {
          console.log('Finished playing!');
          connection.disconnect();
          dispatcher.destroy();
        });
      } else {
        message.channel.send('You need to join a voice channel first!');
      }
      break;
    case '!shoot':
      if (message.member?.voice?.channel) {
        const connection = await message.member.voice.channel.join();
        const hit_sound = moorhun_is_close ? 'audio/chickenhit2.wav' : 'audio/sfx_stone_shot.wav';
        const dispatcher = connection.play(hit_sound);
        dispatcher.on('finish', () => {
          console.log('Finished playing!');
          connection.disconnect();
          dispatcher.destroy();
        });
      } else {
        message.channel.send('You need to join a voice channel first!');
      }
      break;
    case '!poke':
      message.author.send(`Poking ${args[0]}`);
      const msg = args[1] || '';
      const fetchUser = async (id: string) => client.users.fetch(id);
      const user_id = args[0].replace('@', '').replace('<', '').replace('>', '').replace('!', '');
      fetchUser(user_id).then(user => {
        user.send(`poked by ${message.author.username}: ${msg}`);
        console.log(user);
      });
      break;
    case '!meow':
      if (message.member?.voice?.channel) {
        const files: string[] = [];
        fs.readdirSync(mew_sounds_directory).forEach(file => {
          files.push(`${mew_sounds_directory}${file}`);
        });

        const file_index = Math.floor(Math.random() * files.length);
        const random_audio_filepath = files[file_index];
	const p = Math.random();
	console.log(p);
        const final_audio_filpath = p > p_purr_sound ? random_audio_filepath : cat_purring_sound;

        const connection = await message.member.voice.channel.join();
        const dispatcher = connection.play(final_audio_filpath);
        dispatcher.on('finish', () => {
          console.log('Sound finished playing...');
          connection.disconnect();
          dispatcher.destroy();
        });
      } else {
        message.channel.send('You need to join a voice channel first!');
      }
      break;
    default:
      console.log(`Unknown command ${cmd}`);
  }
});

client.login(SIR_PIGEON_DISCORD_TOKEN);
